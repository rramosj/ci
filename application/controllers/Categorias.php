<?php
class Categorias extends CI_Controller{	
	public function __construct(){
		parent::__construct();
		$this->load->model('categoria');		
	}
	//Obtencion de todos los registros
	public function index(){				
		$data['registros'] = $this->categoria->all();
		$this->load->view('categorias',$data);
	}
	//Obtencion de un solo registro
	public function show($id){				
		$data['registro'] = $this->categoria->find($id);
		$this->load->view('categoria',$data);	
	}	
	//Desplegar la informacion existentente en un formulario
	public function edit($id){//GET
		$data['registro'] = $this->categoria->find($id);
		$this->load->view('editar_categoria',$data);	
	}	
	//Actualizar informacion de un registro	
	public function update($id){//POST
		
	}
}