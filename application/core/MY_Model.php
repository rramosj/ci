<?php
class MY_Model extends CI_Model{
	private $id = 'id';	
	public $table = '';

	function all(){
		//Select * from table
		$data = $this->db->get($this->table);
		//result_array
		return $data->result();
	}

	function find($id){
		//Select * from table where id = ?
		$data = $this->db->where($this->id,$id)->get($this->table);
		//result_array
		return $data->row();	
	}
}