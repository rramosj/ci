<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Categorias</title>
</head>
<body>
	<table border="1">
		<thead>
			<tr>
				<th>ID</th>
				<th>NAME</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($registros as $registro){?>
			<tr>
				<td><?php echo $registro->id?></td>
				<td><?php echo $registro->name?></td>
				<td>
					<a href="<?php echo base_url('index.php/categorias/show/'.$registro->id)?>">Ver</a>
				</td>
			</tr>
		<?php }?>
		</tbody>
	</table>
</body>
</html>